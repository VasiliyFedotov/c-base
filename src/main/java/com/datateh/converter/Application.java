package com.datateh.converter;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Application {
    private File rootFolder;

    public static void main(String[] args) {
        try {
            if (args.length != 2){
                System.out.println("--------\tДля корректной работы необходимы 2 параметра\t--------");
                System.out.println("1 - корневая папка входящих мапингов (например: ...ros/rosneft/rn-lnd-replication/repo/src/main/amp/config/alfresco/module/lnd-replication-repo/mappings/)");
                System.out.println("2 - исходящая папка для файла mappings.xml (например: ./ )");
                return;
            }
            new Application(args[0], args[1]);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public Application(String dirRead, String dirWrite) {
        XMLDocument xmlDocument = new XMLDocument();
        this.rootFolder = new File(dirRead);
        if (rootFolder != null && rootFolder.isDirectory()) {
            File[] subFolders = rootFolder.listFiles();
            for (File subFolder : subFolders) {
                if (subFolder.isDirectory()) {
                    xmlDocument.setFolder(subFolder.getName());
                    for (File file : subFolder.listFiles()) {
                        xmlDocument.setFile(file.getName(), decodeFile(file));
                    }
                }
            }
        }
        if (dirWrite.lastIndexOf("//") != dirWrite.length() - 1)
            dirWrite += "//";
        xmlDocument.writeToFile(dirWrite + "mappings.xml");
    }

    private String decodeFile(File file) {
        try {
            byte[] bytes = loadFile(file);
            byte[] encoded = Base64.getEncoder().encode(bytes);
            return new String(encoded, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] loadFile(File file) throws IOException {
        int offset = 0;
        int numRead = 0;
        InputStream is = new FileInputStream(file);
        long length = file.length();
        byte[] bytes = new byte[(int) length];
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }
        is.close();
        return bytes;
    }
}
