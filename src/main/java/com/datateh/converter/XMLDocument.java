package com.datateh.converter;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


public class XMLDocument {
    private Element[] rootV;
    private Element[] folder;
    private Element firstItems;
    private Document document;
    private DocumentBuilder dBuilder;
    private boolean isMode;
    private String content;

    public XMLDocument() {
        this.document = null;
        this.firstItems = null;
        isMode = true;
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            dbFactory.setNamespaceAware(false);
            dBuilder = dbFactory.newDocumentBuilder();
            document = dBuilder.newDocument();
            firstItems = document.createElement("items");
            document.appendChild(firstItems);
            Element root = bodyItem(firstItems, "Конфигурация XML", "folder", null);
            rootV = new Element[2];
            rootV[1] = bodyItem(root, "1.0", "folder", null);
            rootV[0] = bodyItem(root, "default", "folder", null);
        } catch (Exception e) {
            System.out.print("generate xml : \t");
            e.printStackTrace();
        }
    }

    public void setFolder(String folderName) {
        folder = new Element[2];
        folder[0] = bodyItem(rootV[0], folderName, "folder", true);
        folder[1] = bodyItem(rootV[1], folderName, "folder", true);
    }

    public void setFile(String fileName, String bytes) {
        content = bytes;
        bodyItem(folder[0], fileName, "content", false);
        bodyItem(folder[1], fileName, "content", false);
    }

    private Element bodyItem(Element root, String name, String type, Boolean isTitle) {
        Element item = createItem(root, name, type);
        Element pItem;
        createProp(item, "name", name);
        if (isTitle != null) {
            if (isTitle) {
                createProp(item, "title", "");
            } else {
                createProp(item, type, content);
            }
        }
        return createItems(item);
    }

    private void createProp(Element root, String name, String pContext) {
        Element pItem = document.createElement("property");
        pItem.setAttribute("name", "cm:" + name);
        pItem.setTextContent(pContext);
        root.appendChild(pItem);
    }

    private Element createItem(Element root, String name, String type) {
        Element item = document.createElement("item");
        item.setAttribute("name", "cm:" + name);
        item.setAttribute("type", "cm:" + type);
        root.appendChild(item);
        return item;
    }

    private Element createItems(Element root) {
        Element item = document.createElement("items");
        if (isMode) {
            item.setAttribute("updateMode", "RewriteChildren");
            isMode = false;
        }
        root.appendChild(item);
        return item;
    }

    public void writeToFile(String xmlFile) {
        try {
            Transformer tr = TransformerFactory.newInstance().newTransformer();
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.VERSION, "1.0");
            tr.setOutputProperty(OutputKeys.METHOD, "xml");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tr.setOutputProperty(OutputKeys.CDATA_SECTION_ELEMENTS, "property");
            tr.transform(new DOMSource(document), new StreamResult(new FileOutputStream(xmlFile)));
        } catch (TransformerException te) {
            System.out.println(te.getMessage());
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }
}
